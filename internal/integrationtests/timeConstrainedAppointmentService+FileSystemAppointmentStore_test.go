package integrationtests

import (
	"fmt"
	"future-appointment-service/internal/infra/dataproviders/storage/fsstorage"
	"future-appointment-service/internal/model"
	"future-appointment-service/internal/services/appointmentservice"
	"future-appointment-service/pkg/clocktime"
	"future-appointment-service/pkg/timerange"
	"math/rand"
	"os"
	"sort"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

const (
	datastoreOutputFilepath = "IntegrationTest-TimeConstrainedAppointmentService-FileSystemAppointmentStore.json"
	timeFormat              = "2006-01-02T15:04:05-07:00"
)

func TestTimeConstrainedAppointmentServiceWithFileSystemAppointmentStore(t *testing.T) {
	var (
		businessDayStartHour = 8
		businessDayEndHour   = 17
		dayStart, _          = clocktime.New(businessDayStartHour, 0) // ignoring error for hard coded valid time
		dayEnd, _            = clocktime.New(businessDayEndHour, 0)   // ignoring error for hard coded valid time
		businessHours        = model.DailyTimeRanges{
			time.Monday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Tuesday:   clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Wednesday: clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Thursday:  clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Friday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Saturday:  clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Sunday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
		}
		appointmentDuration                    = time.Minute * 30
		validStartMinutes                      = []int{0, 30}
		timeConstraints                        = model.NewTimeRangeConstraints(businessHours, appointmentDuration, validStartMinutes)
		now                                    = time.Now()
		referenceTime                          = time.Date(now.Year(), now.Month(), now.Day(), dayStart.TimeRelativeTo(now).Hour(), 0, 0, 0, now.Location())
		scheduleAppointmentTestUserID          = 1
		scheduleAppointmentTestTrainerID       = 2
		scheduledAppointmentsTestUserID        = 3
		scheduledAppointmentsTestTrainerID     = 4
		availableAppointmentTimesTestUserID    = 5
		availableAppointmentTimesTestTrainerID = 6
	)

	// FileSystemAppointmentStore expects that the file exists
	_, err := os.Create(datastoreOutputFilepath)
	require.Nil(t, err)
	defer os.Remove(datastoreOutputFilepath)

	// Setup AppointmentStore datastore
	datastore, err := fsstorage.NewFileSystemAppointmentStore(datastoreOutputFilepath, timeFormat)
	require.Nil(t, err)

	// Setup AppointmentService service
	timeConstrainedAppointmentService := appointmentservice.NewTimeConstrainedAppointmentService(timeConstraints, datastore)

	t.Run("ScheduleAppointment", func(t *testing.T) {
		var (
			userID                        = scheduleAppointmentTestUserID
			trainerID                     = scheduleAppointmentTestTrainerID
			startTime                     = referenceTime
			endTime                       = referenceTime.Add(appointmentDuration)
			referenceAppointment          = model.NewAppointment(userID, trainerID, startTime, endTime)
			referenceScheduledAppointment = model.Appointment{}
		)
		// Valid Appointment: OK
		appointment := referenceAppointment
		scheduledAppointment, err := timeConstrainedAppointmentService.ScheduleAppointment(appointment)
		require.Nil(t, err)
		require.NotEmpty(t, scheduledAppointment.ID)
		appointment.ID = scheduledAppointment.ID
		require.Equal(t, appointment, scheduledAppointment)
		referenceScheduledAppointment = scheduledAppointment

		// Duplicate Appointment: NOK
		appointment = referenceAppointment
		_, err = timeConstrainedAppointmentService.ScheduleAppointment(appointment)
		require.NotNil(t, err)

		// Same schedule, different trainer: OK
		appointment = referenceAppointment
		appointment.TrainerID = 99999
		scheduledAppointment, err = timeConstrainedAppointmentService.ScheduleAppointment(appointment)
		require.Nil(t, err)
		require.NotEmpty(t, scheduledAppointment.ID)
		appointment.ID = scheduledAppointment.ID
		require.Equal(t, appointment, scheduledAppointment)

		// Appointment starting immediately after: OK
		// TODO: FIX: brittle - relies on the assumption that appointmentDuration and startMinutes match
		if contains(validStartMinutes, int(appointmentDuration.Minutes())) {
			appointment = referenceAppointment
			appointment.StartedAt = referenceScheduledAppointment.EndedAt
			appointment.EndedAt = appointment.StartedAt.Add(appointmentDuration)
			scheduledAppointment, err = timeConstrainedAppointmentService.ScheduleAppointment(appointment)
			require.Nil(t, err)
			require.NotEmpty(t, scheduledAppointment.ID)
			appointment.ID = scheduledAppointment.ID
			require.Equal(t, appointment, scheduledAppointment)
		}

		// Appointment starting 1h after (still has valid minutes)
		appointment = referenceAppointment
		appointment.StartedAt = referenceScheduledAppointment.StartedAt.Add(time.Hour)
		appointment.EndedAt = appointment.StartedAt.Add(appointmentDuration)
		scheduledAppointment, err = timeConstrainedAppointmentService.ScheduleAppointment(appointment)
		require.Nil(t, err)
		require.NotEmpty(t, scheduledAppointment.ID)
		appointment.ID = scheduledAppointment.ID
		require.Equal(t, appointment, scheduledAppointment)

		// Overlapping Appointment: NOK
		appointment = referenceAppointment
		appointment.StartedAt = appointment.StartedAt.Add(time.Duration(validStartMinutes[len(validStartMinutes)-1]))
		appointment.EndedAt = appointment.StartedAt.Add(appointmentDuration)
		_, err = timeConstrainedAppointmentService.ScheduleAppointment(appointment)
		require.NotNil(t, err)

		// Invalid duration
		appointment = referenceAppointment
		appointment.StartedAt = startTime
		appointment.EndedAt = startTime.Add(appointmentDuration + 1)
		_, err = timeConstrainedAppointmentService.ScheduleAppointment(appointment)
		require.NotNil(t, err)

		// Invalid start minutes
		appointment = referenceAppointment
		appointment.StartedAt = startTime.Add(time.Minute)
		appointment.EndedAt = startTime.Add(appointmentDuration)
		_, err = timeConstrainedAppointmentService.ScheduleAppointment(appointment)
		require.NotNil(t, err)

		// Outside business hours
		appointment = referenceAppointment
		appointment.StartedAt = startTime.Add(-time.Hour)
		appointment.EndedAt = startTime.Add(appointmentDuration)
		_, err = timeConstrainedAppointmentService.ScheduleAppointment(appointment)
		require.NotNil(t, err)
	})

	t.Run("ScheduledAppointments", func(t *testing.T) {
		var (
			userID    = scheduledAppointmentsTestUserID
			trainerID = scheduledAppointmentsTestTrainerID
		)

		// No appointments
		scheduledAppointments, err := timeConstrainedAppointmentService.GetScheduledAppointmentsForTrainer(trainerID)
		require.Nil(t, err)
		require.Empty(t, scheduledAppointments)

		// Schedule some appointments
		appointments := []model.Appointment{}
		for i := 0; i < (businessDayEndHour - businessDayStartHour); i++ {
			appointmentStartTime := referenceTime.Add(time.Hour * time.Duration(i))
			appointmentEndTime := appointmentStartTime.Add(appointmentDuration)
			appointments = append(appointments, model.NewAppointment(userID, trainerID, appointmentStartTime, appointmentEndTime))
		}

		for i, appointment := range appointments {
			scheduledAppointment, err := timeConstrainedAppointmentService.ScheduleAppointment(appointment)
			require.Nil(t, err)
			require.NotEmpty(t, scheduledAppointment.ID)
			appointment.ID = scheduledAppointment.ID
			require.Equal(t, appointment, scheduledAppointment)

			appointments[i].ID = appointment.ID
		}

		// Fetch & check scheduled appointments
		scheduledAppointments, err = timeConstrainedAppointmentService.GetScheduledAppointmentsForTrainer(trainerID)
		require.Nil(t, err)
		sort.Slice(scheduledAppointments, func(i, j int) bool {
			return scheduledAppointments[i].StartedAt.Before(scheduledAppointments[j].StartedAt)
		})
		sort.Slice(appointments, func(i, j int) bool {
			return appointments[i].StartedAt.Before(appointments[j].StartedAt)
		})
		require.Equal(t, scheduledAppointments, appointments)
	})

	t.Run("AvailableAppointmentTimes", func(t *testing.T) {
		var (
			userID              = availableAppointmentTimesTestUserID
			trainerID           = availableAppointmentTimesTestTrainerID
			availableRangeStart = time.Date(now.Year(), now.Month(), now.Day(), dayStart.TimeRelativeTo(now).Hour(), 0, 0, 0, now.Location())
			availableRangeEnd   = time.Date(now.Year(), now.Month(), now.Day(), dayEnd.TimeRelativeTo(now).Hour(), 0, 0, 0, now.Location())
			timeRange           = timerange.NewTimeRange(availableRangeStart.Add(-time.Hour), availableRangeStart.Add(time.Hour*24))
		)

		timeToNextValidStartTime := func(currentStartTime time.Time) (time.Duration, error) {
			for i, validStartMinute := range validStartMinutes {
				if validStartMinute == currentStartTime.Minute() {
					if i == len(validStartMinutes)-1 {
						timeToAdd := (60 - currentStartTime.Minute()) + (validStartMinutes[0])
						return time.Duration(timeToAdd) * time.Minute, nil
					}
					timeToAdd := validStartMinutes[i+1] - validStartMinute
					return time.Duration(timeToAdd) * time.Minute, nil
				}
			}
			return 0, fmt.Errorf("invalid current time")
		}

		// Generate expected available times in range
		expectedAvailableAppointmentTimes := []timerange.TimeRange{}
		start := availableRangeStart
		for start.Before(availableRangeEnd) {
			availableAppointmentTime := timerange.NewTimeRange(start, start.Add(appointmentDuration))
			expectedAvailableAppointmentTimes = append(expectedAvailableAppointmentTimes, availableAppointmentTime)

			timeToNextStart, err := timeToNextValidStartTime(start)
			require.Nil(t, err)
			start = start.Add(timeToNextStart)
			t.Log(timeToNextStart)
		}

		// Fetch available times in ragne
		actualAvailableAppointmentTimes, err := timeConstrainedAppointmentService.GetAvailableAppointmentsTimesForTrainerInTimeRange(trainerID, timeRange.Start, timeRange.End)
		require.Nil(t, err)

		// Ensure expected matches actual
		sort.Slice(expectedAvailableAppointmentTimes, func(i, j int) bool {
			return expectedAvailableAppointmentTimes[i].Start.Before(expectedAvailableAppointmentTimes[j].Start)
		})
		sort.Slice(actualAvailableAppointmentTimes, func(i, j int) bool {
			return actualAvailableAppointmentTimes[i].Start.Before(actualAvailableAppointmentTimes[j].Start)
		})
		require.Equal(t, expectedAvailableAppointmentTimes, actualAvailableAppointmentTimes)

		// Fill available spots one by one
		scheduledAppointments := []model.Appointment{}
		for len(actualAvailableAppointmentTimes) > 0 {
			// Pick random appointment
			randomAppointmentTimeSlot := actualAvailableAppointmentTimes[rand.Intn(len(actualAvailableAppointmentTimes))]
			appointmentForRandomTime := model.NewAppointment(userID, trainerID, randomAppointmentTimeSlot.Start, randomAppointmentTimeSlot.End)

			// Schedule it
			scheduledAppointment, err := timeConstrainedAppointmentService.ScheduleAppointment(appointmentForRandomTime)
			require.Nil(t, err)
			appointmentForRandomTime.ID = scheduledAppointment.ID
			require.Equal(t, appointmentForRandomTime, scheduledAppointment)
			scheduledAppointments = append(scheduledAppointments, scheduledAppointment)

			// Remove it from expected available times
			for i := len(expectedAvailableAppointmentTimes) - 1; i >= 0; i-- {
				if expectedAvailableAppointmentTimes[i].Start == scheduledAppointment.StartedAt &&
					expectedAvailableAppointmentTimes[i].End == scheduledAppointment.EndedAt {
					expectedAvailableAppointmentTimes = append(expectedAvailableAppointmentTimes[:i], expectedAvailableAppointmentTimes[i+1:]...)
				}
			}

			// Check availability again, make sure the scheduled appointment is no longer available
			actualAvailableAppointmentTimes, err = timeConstrainedAppointmentService.GetAvailableAppointmentsTimesForTrainerInTimeRange(trainerID, timeRange.Start, timeRange.End)
			require.Nil(t, err)

			sort.Slice(expectedAvailableAppointmentTimes, func(i, j int) bool {
				return expectedAvailableAppointmentTimes[i].Start.Before(expectedAvailableAppointmentTimes[j].Start)
			})
			sort.Slice(actualAvailableAppointmentTimes, func(i, j int) bool {
				return actualAvailableAppointmentTimes[i].Start.Before(actualAvailableAppointmentTimes[j].Start)
			})
			require.Equal(t, expectedAvailableAppointmentTimes, actualAvailableAppointmentTimes)
		}

		datastoreScheduledAppointments, err := timeConstrainedAppointmentService.GetScheduledAppointmentsForTrainer(trainerID)
		require.Nil(t, err)
		require.Equal(t, scheduledAppointments, datastoreScheduledAppointments)
	})
}

func contains(values []int, value int) bool {
	for _, v := range values {
		if v == value {
			return true
		}
	}
	return false
}
