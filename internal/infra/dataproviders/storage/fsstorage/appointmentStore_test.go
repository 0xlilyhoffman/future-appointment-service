package fsstorage

import (
	"future-appointment-service/internal/model"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

const (
	timeFormat = "2006-01-02T15:04:05-07:00"
	filepath   = "fsstorage-test-out.json"
)

func TestFileSystemAppointmentStore(t *testing.T) {
	var (
		now       = time.Now()
		startTime = time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 0, 0, 0, now.Location())
		endTime   = startTime.Add(time.Hour)
		userID    = 1
		trainerID = 2
		a         = model.NewAppointment(userID, trainerID, startTime, endTime)
	)

	// FileSystemAppointmentStore expects that the file exists
	_, err := os.Create(filepath)
	require.Nil(t, err)
	defer os.Remove(filepath)

	appointmentStore, err := NewFileSystemAppointmentStore(filepath, timeFormat)
	require.Nil(t, err)

	t.Run("AddAppointment", func(t *testing.T) {
		id, err := appointmentStore.AddAppointment(a)
		require.Nil(t, err)
		require.NotEmpty(t, id)
		a.ID = id
	})

	t.Run("GetAppointment", func(t *testing.T) {
		appointment, err := appointmentStore.GetAppointment(a.ID)
		require.Nil(t, err)
		require.Equal(t, a, appointment)

		appointment, err = appointmentStore.GetAppointment(a.ID + 1)
		require.NotNil(t, err)
	})

	t.Run("GetAppointments", func(t *testing.T) {
		appointments, err := appointmentStore.GetAppointments(model.AppointmentFilter{})
		require.Nil(t, err)
		require.Len(t, appointments, 1)

		appointments, err = appointmentStore.GetAppointments(model.AppointmentFilter{
			UserID:    userID,
			TrainerID: trainerID,
			StartedAt: startTime,
			EndedAt:   endTime,
		})
		require.Nil(t, err)
		require.Len(t, appointments, 1)

		appointments, err = appointmentStore.GetAppointments(model.AppointmentFilter{
			UserID: userID + 1,
		})
		require.Nil(t, err)
		require.Len(t, appointments, 0)

	})
}
