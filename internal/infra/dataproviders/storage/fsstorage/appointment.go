package fsstorage

import (
	"future-appointment-service/internal/model"
	"time"
)

type FileSystemAppointment struct {
	ID        int    `json:"id"`
	UserID    int    `json:"user_id"`
	TrainerID int    `json:"trainer_id"`
	StartedAt string `json:"started_at"`
	EndedAt   string `json:"ended_at"`
}

func newFileSystemAppointment(a model.Appointment, timeFormat string) FileSystemAppointment {
	return FileSystemAppointment{
		ID:        a.ID,
		UserID:    a.UserID,
		TrainerID: a.TrainerID,
		StartedAt: a.StartedAt.Format(timeFormat),
		EndedAt:   a.EndedAt.Format(timeFormat),
	}
}

func (fsa FileSystemAppointment) appointment(timeFormat string) (model.Appointment, error) {
	startedAtTime, err := time.Parse(timeFormat, fsa.StartedAt)
	if err != nil {
		return model.Appointment{}, err
	}

	endedAtTime, err := time.Parse(timeFormat, fsa.EndedAt)
	if err != nil {
		return model.Appointment{}, err
	}

	a := model.Appointment{
		ID:        fsa.ID,
		UserID:    fsa.UserID,
		TrainerID: fsa.TrainerID,
		StartedAt: startedAtTime,
		EndedAt:   endedAtTime,
	}

	return a, nil
}
