package fsstorage

import (
	"encoding/json"
	"errors"
	"fmt"
	"future-appointment-service/internal/model"
	"future-appointment-service/internal/services/appointmentservice"
	"io/fs"
	"os"
)

var _ appointmentservice.AppointmentStore = (FileSystemAppointmentStore)(FileSystemAppointmentStore{})

type FileSystemAppointmentStore struct {
	filepath   string
	timeFormat string
}

func NewFileSystemAppointmentStore(filepath string, timeFormat string) (FileSystemAppointmentStore, error) {
	_, err := os.Stat(filepath)
	if errors.Is(err, os.ErrNotExist) {
		return FileSystemAppointmentStore{}, err
	}

	store := FileSystemAppointmentStore{
		filepath:   filepath,
		timeFormat: timeFormat,
	}
	return store, nil
}

func (s FileSystemAppointmentStore) AddAppointment(a model.Appointment) (int, error) {
	fsa := newFileSystemAppointment(a, s.timeFormat)
	return s.append(fsa)
}

func (s FileSystemAppointmentStore) GetAppointments(filter model.AppointmentFilter) ([]model.Appointment, error) {
	fsAppointments, err := s.read()
	if err != nil {
		return nil, err
	}

	appointments := []model.Appointment{}
	for _, fsAppointment := range fsAppointments {
		appointment, err := fsAppointment.appointment(s.timeFormat)
		if err != nil {
			continue
		}

		if !filter.Matches(appointment) {
			continue
		}

		appointments = append(appointments, appointment)
	}

	return appointments, nil
}

func (s FileSystemAppointmentStore) GetAppointment(id int) (model.Appointment, error) {
	var (
		appointment model.Appointment
		err         error
	)
	fsAppointments, err := s.read()
	if err != nil {
		return appointment, err
	}

	for _, fsAppointment := range fsAppointments {
		if fsAppointment.ID == id {
			appointment, err := fsAppointment.appointment(s.timeFormat)
			if err != nil {
				return appointment, err
			}

			return appointment, nil
		}
	}

	return model.Appointment{}, fmt.Errorf("appointment with id %d not found", id)
}

func (s FileSystemAppointmentStore) read() ([]FileSystemAppointment, error) {
	data, err := os.ReadFile(s.filepath)
	if err != nil {
		return nil, err
	}

	if len(data) == 0 {
		return nil, nil
	}

	appointments := []FileSystemAppointment{}
	err = json.Unmarshal(data, &appointments)
	if err != nil {
		return nil, err
	}

	return appointments, nil
}

func (s FileSystemAppointmentStore) write(fsAppointments []FileSystemAppointment) error {
	data, err := json.MarshalIndent(fsAppointments, "", "\t")
	if err != nil {
		return err
	}

	mode := fs.FileMode(0664) // TODO: revisit
	err = os.WriteFile(s.filepath, data, mode)
	if err != nil {
		return err
	}

	return nil
}

// TODO: append without reading/writing entire file again - must be careful about "," and "]" at the end of the json file, also must find last id
func (s FileSystemAppointmentStore) append(fsAppointment FileSystemAppointment) (int, error) {
	appointments, err := s.read()
	if err != nil {
		return 0, err
	}

	id := len(appointments) + 1
	fsAppointment.ID = id
	appointments = append(appointments, fsAppointment)

	err = s.write(appointments)
	if err != nil {
		return 0, err
	}

	return id, nil
}
