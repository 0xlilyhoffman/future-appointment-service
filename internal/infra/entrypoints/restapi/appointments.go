package restapi

import (
	"encoding/json"
	"fmt"
	"future-appointment-service/internal/model"
	"future-appointment-service/internal/services/appointmentservice"
	"future-appointment-service/pkg/timerange"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/hashicorp/go-multierror"
)

const (
	URLParamTrainerID = "trainer_id"
	URLParamStartsAt  = "starts_at"
	URLParamEndsAt    = "ends_at"
)

type AppointmentService struct {
	router     *chi.Mux
	scheduler  appointmentservice.AppointmentService
	timeFormat string
}

func NewAppointmnetService(scheduler appointmentservice.AppointmentService, timeFormat string) AppointmentService {
	s := AppointmentService{
		router:     chi.NewRouter(),
		scheduler:  scheduler,
		timeFormat: timeFormat,
	}
	s.setupRoutes()
	return s
}

func (s AppointmentService) setupRoutes() {
	s.router.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.Logger,
		middleware.Recoverer,
	)
	s.router.Get("/health", s.HealthCheck)
	s.router.Route("/appointments", func(r chi.Router) {
		r.Post("/", s.CreateAppointment)
		r.Route(fmt.Sprintf("/{%s}", URLParamTrainerID), func(r chi.Router) {
			r.Get("/scheduled", s.GetScheduledAppointmentsForTrainer)
			r.Route(fmt.Sprintf("/available/{%s}/{%s}", URLParamStartsAt, URLParamEndsAt), func(r chi.Router) {
				r.Get("/", s.GetAvailableAppointmentTimesForTrainerInTimeRange)
			})
		})
	})
}

func (s AppointmentService) ListenAndServe(address string) error {
	return http.ListenAndServe(address, s.router)
}

func (s AppointmentService) HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("Healthy"))
}

type CreateAppointmentRequest struct {
	TrainerID int    `json:"trainer_id"`
	UserID    int    `json:"user_id"`
	StartsAt  string `json:"starts_at"`
	EndsAt    string `json:"ends_at"`
}

func (r CreateAppointmentRequest) Validate() error {
	var result error

	requiredIntFields := map[string]int{
		"TrainerID": r.TrainerID,
		"UserID":    r.UserID,
	}

	requiredStringFields := map[string]string{
		"StartsAt": r.StartsAt,
		"EndsAt":   r.EndsAt,
	}

	for k, v := range requiredStringFields {
		if v == "" {
			result = multierror.Append(result, fmt.Errorf("missing required field: %s", k))
		}
	}
	for k, v := range requiredIntFields {
		if v == 0 {
			result = multierror.Append(result, fmt.Errorf("missing required field: %s", k))
		}
	}

	return result
}

type CreateAppointmentResponse struct {
	Appointment model.Appointment `json:"appointment"`
}

func (s AppointmentService) CreateAppointment(w http.ResponseWriter, r *http.Request) {
	var (
		req  CreateAppointmentRequest
		resp CreateAppointmentResponse
		err  error
	)

	err = json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	err = req.Validate()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	startAtTime, err := time.Parse(s.timeFormat, req.StartsAt)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	endAtTime, err := time.Parse(s.timeFormat, req.EndsAt)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	appointment := model.NewAppointment(req.UserID, req.TrainerID, startAtTime, endAtTime)
	scheduledAppointment, err := s.scheduler.ScheduleAppointment(appointment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp = CreateAppointmentResponse{
		Appointment: scheduledAppointment,
	}

	WriteJSON(w, http.StatusCreated, &resp)
}

type AvailableAppointmentTimesResponse struct {
	AppointmentTimes []timerange.TimeRange `json:"appointmentTimes"`
}

func (s AppointmentService) GetAvailableAppointmentTimesForTrainerInTimeRange(w http.ResponseWriter, r *http.Request) {
	var (
		trainerIDParam = chi.URLParam(r, URLParamTrainerID)
		startsAtParam  = chi.URLParam(r, URLParamStartsAt)
		endsAtParam    = chi.URLParam(r, URLParamEndsAt)
		resp           AvailableAppointmentTimesResponse
	)

	if trainerIDParam == "" || startsAtParam == "" || endsAtParam == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	trainerID, err := strconv.Atoi(trainerIDParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	startsAt, err := time.Parse(s.timeFormat, startsAtParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	endsAt, err := time.Parse(s.timeFormat, endsAtParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	appointmentTimes, err := s.scheduler.GetAvailableAppointmentsTimesForTrainerInTimeRange(trainerID, startsAt, endsAt)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp = AvailableAppointmentTimesResponse{
		AppointmentTimes: appointmentTimes,
	}

	WriteJSON(w, http.StatusOK, &resp)
}

type AppointmentsResponse struct {
	Appointments []model.Appointment `json:"appointments"`
}

func (s AppointmentService) GetScheduledAppointmentsForTrainer(w http.ResponseWriter, r *http.Request) {
	var (
		trainerIDParam = chi.URLParam(r, URLParamTrainerID)
		resp           AppointmentsResponse
	)

	if trainerIDParam == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	trainerID, err := strconv.Atoi(trainerIDParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	appointments, err := s.scheduler.GetScheduledAppointmentsForTrainer(trainerID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp = AppointmentsResponse{
		Appointments: appointments,
	}

	WriteJSON(w, http.StatusOK, &resp)
}
