package restapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"future-appointment-service/internal/model"
	"future-appointment-service/internal/services/appointmentservice"
	"future-appointment-service/pkg/timerange"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/go-chi/chi"
	"github.com/stretchr/testify/require"
)

const (
	timeFormat = "2006-01-02T15:04:05-07:00"
)

var _ appointmentservice.AppointmentService = (MockAppointmentService)(MockAppointmentService{})

type MockAppointmentService struct{}

func (m MockAppointmentService) ScheduleAppointment(a model.Appointment) (model.Appointment, error) {
	return model.Appointment{}, nil
}

func (m MockAppointmentService) GetScheduledAppointmentsForTrainer(trainerID int) ([]model.Appointment, error) {
	return nil, nil
}

func (m MockAppointmentService) GetAvailableAppointmentsTimesForTrainerInTimeRange(trainerID int, startTime time.Time, endTime time.Time) ([]timerange.TimeRange, error) {
	return nil, nil
}

func TestAppointmentsAPI(t *testing.T) {
	var (
		service   = NewAppointmnetService(MockAppointmentService{}, timeFormat)
		now       = time.Now()
		startTime = time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 0, 0, 0, now.Location())
		endTime   = startTime.Add(time.Hour)
		userID    = 1
		trainerID = 2
		a         = model.NewAppointment(userID, trainerID, startTime, endTime)
	)

	t.Run("CreateAppointment", func(t *testing.T) {
		var (
			method = http.MethodPost
			path   = "/appointments"
			req    = CreateAppointmentRequest{
				TrainerID: a.TrainerID,
				UserID:    a.UserID,
				StartsAt:  a.StartedAt.Format(service.timeFormat),
				EndsAt:    a.EndedAt.Format(service.timeFormat),
			}
			resp = CreateAppointmentResponse{}
		)
		w := sendJSONRequest(t, service.router, method, path, req)
		require.Equal(t, http.StatusCreated, w.Code)

		err := json.Unmarshal(w.Body.Bytes(), &resp)
		require.Nil(t, err)

		req.TrainerID = 0
		w = sendJSONRequest(t, service.router, method, path, req)
		require.Equal(t, http.StatusBadRequest, w.Code)

		req.UserID = 0
		w = sendJSONRequest(t, service.router, method, path, req)
		require.Equal(t, http.StatusBadRequest, w.Code)

		req.StartsAt = ""
		w = sendJSONRequest(t, service.router, method, path, req)
		require.Equal(t, http.StatusBadRequest, w.Code)

		req.EndsAt = ""
		w = sendJSONRequest(t, service.router, method, path, req)
		require.Equal(t, http.StatusBadRequest, w.Code)

	})
	t.Run("GetScheduledAppointmentsForTrainer", func(t *testing.T) {
		var (
			method = http.MethodGet
			path   = fmt.Sprintf("/appointments/%d/scheduled", a.TrainerID)
			req    = struct{}{}
			resp   = AppointmentsResponse{}
		)
		w := sendJSONRequest(t, service.router, method, path, req)
		require.Equal(t, http.StatusOK, w.Code)

		err := json.Unmarshal(w.Body.Bytes(), &resp)
		require.Nil(t, err)

	})
	t.Run("GetAvailableAppointmentTimesForTrainerInTimeRange", func(t *testing.T) {
		var (
			method = http.MethodGet
			path   = fmt.Sprintf("/appointments/%d/available/%s/%s", a.TrainerID, a.StartedAt.Format(service.timeFormat), a.EndedAt.Format(service.timeFormat))
			req    = struct{}{}
			resp   = AvailableAppointmentTimesResponse{}
		)
		w := sendJSONRequest(t, service.router, method, path, req)
		require.Equal(t, http.StatusOK, w.Code)

		err := json.Unmarshal(w.Body.Bytes(), &resp)
		require.Nil(t, err)
	})
}

func sendJSONRequest(t *testing.T, router *chi.Mux, method string, path string, req interface{}) *httptest.ResponseRecorder {
	payload, err := json.Marshal(req)
	if err != nil {
		t.Errorf("marshal request payload failed: %v", err)
	}

	request, err := http.NewRequest(method, path, bytes.NewReader(payload))
	if err != nil {
		t.Errorf("new request for %s %s %+v failed: %v", method, path, req, err)
	}
	request.Header.Set("Content-Type", "application/json")

	w := httptest.NewRecorder()
	router.ServeHTTP(w, request)
	return w
}
