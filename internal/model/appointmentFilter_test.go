package model

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestAppointmentFilter(t *testing.T) {
	now := time.Now()
	referenceTime := time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 0, 0, 0, now.Location())
	appointmentDuration := time.Minute * 30

	userID := 1
	trainerID := 2
	appointmentStart := referenceTime
	appointmentEnd := referenceTime.Add(appointmentDuration)
	referenceAppointment := NewAppointment(userID, trainerID, appointmentStart, appointmentEnd)

	t.Run("Matches", func(t *testing.T) {
		var (
			validateMatch = func(t *testing.T, filter AppointmentFilter) {
				matches := filter.Matches(referenceAppointment)
				require.True(t, matches)
			}
			validateNoMatch = func(t *testing.T, filter AppointmentFilter) {
				matches := filter.Matches(referenceAppointment)
				require.False(t, matches)
			}
		)

		validateMatch(t, AppointmentFilter{
			UserID:    userID,
			TrainerID: trainerID,
			StartedAt: appointmentStart,
			EndedAt:   appointmentEnd,
		})
		validateMatch(t, AppointmentFilter{
			UserID:            userID,
			TrainerID:         trainerID,
			StartedAt:         appointmentStart,
			StartedAtOrBefore: appointmentStart,
			StartedAtOrAfter:  appointmentStart,
			EndedAt:           appointmentEnd,
			EndedAtOrBefore:   appointmentEnd,
			EndedAtOrAfter:    appointmentEnd,
		})

		validateMatch(t, AppointmentFilter{
			UserID: userID,
		})
		validateMatch(t, AppointmentFilter{
			TrainerID: trainerID,
		})
		validateMatch(t, AppointmentFilter{
			StartedAt: appointmentStart,
		})
		validateMatch(t, AppointmentFilter{
			EndedAt: appointmentEnd,
		})
		validateMatch(t, AppointmentFilter{
			StartedAtOrBefore: appointmentStart,
		})
		validateMatch(t, AppointmentFilter{
			StartedAtOrAfter: appointmentStart,
		})
		validateMatch(t, AppointmentFilter{
			EndedAtOrBefore: appointmentEnd,
		})
		validateMatch(t, AppointmentFilter{
			EndedAtOrAfter: appointmentEnd,
		})

		validateNoMatch(t, AppointmentFilter{
			UserID: userID + 1,
		})
		validateNoMatch(t, AppointmentFilter{
			UserID:    userID,
			TrainerID: trainerID + 1,
		})
	})
}
