package model

import (
	"sort"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestNextTimeWithMinutesIn(t *testing.T) {
	t.Run("NextTimeWithMinutesIn", func(t *testing.T) {
		now := time.Now()
		minutes := []int{0, 30}
		require.True(t, sort.IntsAreSorted(minutes))
		require.Equal(t, len(minutes), 2)

		type MinutesTestCase struct {
			value    time.Time
			expected time.Time
		}

		testCases := []MinutesTestCase{
			{
				// valid
				value:    time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), minutes[0], 0, 0, now.Location()),
				expected: time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), minutes[0], 0, 0, now.Location()),
			},
			{
				// invalid - first time is one minute off - requires minute update to next valid time
				value:    time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), minutes[0]+1, 0, 0, now.Location()),
				expected: time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), minutes[1], 0, 0, now.Location()),
			},
			{
				// invalid - first time is 59 minutes off - requires minute and hour update
				value:    time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), minutes[0]+59, 0, 0, now.Location()),
				expected: time.Date(now.Year(), now.Month(), now.Day(), now.Hour()+1, minutes[0], 0, 0, now.Location()),
			},
		}

		for _, testCase := range testCases {
			result, err := Time(testCase.value).NextTimeWithMinutesIn(minutes)
			require.Nil(t, err)
			require.Equal(t, testCase.expected, result)
		}

		_, err := Time(now).NextTimeWithMinutesIn([]int{})
		require.NotNil(t, err)
	})
}
