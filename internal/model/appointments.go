package model

import "future-appointment-service/pkg/timerange"

type Appointments []Appointment

func (as Appointments) Times() []timerange.TimeRange {
	times := []timerange.TimeRange{}
	for _, a := range as {
		times = append(times, timerange.NewTimeRange(a.StartedAt, a.EndedAt))
	}
	return times
}
