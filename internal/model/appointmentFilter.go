package model

import "time"

type AppointmentFilter struct {
	UserID            int
	TrainerID         int
	StartedAt         time.Time
	StartedAtOrBefore time.Time
	StartedAtOrAfter  time.Time
	EndedAt           time.Time
	EndedAtOrBefore   time.Time
	EndedAtOrAfter    time.Time
}

func (f AppointmentFilter) Matches(a Appointment) bool {
	var (
		userIDRequired            = f.UserID != 0
		trainerIDRequired         = f.TrainerID != 0
		startedAtRequired         = f.StartedAt != time.Time{}
		startedAtOrBeforeRequired = f.StartedAtOrBefore != time.Time{}
		startedAtOrAfterRequired  = f.StartedAtOrAfter != time.Time{}
		endedAtRequired           = f.EndedAt != time.Time{}
		endedBeforeRequired       = f.EndedAtOrBefore != time.Time{}
		endedAfterRequired        = f.EndedAtOrAfter != time.Time{}

		userIDMatches            = f.UserID == a.UserID
		trainerIDMatches         = f.TrainerID == a.TrainerID
		startedAtMatches         = f.StartedAt == a.StartedAt
		startedAtOrBeforeMatches = a.StartedAt.Before(f.StartedAtOrBefore) || a.StartedAt == f.StartedAtOrBefore
		startedAtOrAfterMatches  = a.StartedAt.After(f.StartedAtOrAfter) || a.StartedAt == f.StartedAtOrAfter
		endedAtMatches           = f.EndedAt == a.EndedAt
		endedBeforeMatches       = a.EndedAt.Before(f.EndedAtOrBefore) || a.EndedAt == f.EndedAtOrBefore
		endedAfterMatches        = a.EndedAt.After(f.EndedAtOrAfter) || a.EndedAt == f.EndedAtOrAfter
	)

	if (userIDRequired && !userIDMatches) ||
		(trainerIDRequired && !trainerIDMatches) ||
		(startedAtRequired && !startedAtMatches) ||
		(endedAtRequired && !endedAtMatches) ||
		(startedAtOrBeforeRequired && !startedAtOrBeforeMatches) ||
		(startedAtOrAfterRequired && !startedAtOrAfterMatches) ||
		(endedBeforeRequired && !endedBeforeMatches) ||
		(endedAfterRequired && !endedAfterMatches) {
		return false
	}

	return true
}
