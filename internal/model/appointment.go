package model

import (
	"future-appointment-service/pkg/timerange"
	"time"
)

type Appointment struct {
	ID        int
	UserID    int
	TrainerID int
	StartedAt time.Time
	EndedAt   time.Time
}

func NewAppointment(userID int, trainerID int, start time.Time, end time.Time) Appointment {
	return Appointment{
		UserID:    userID,
		TrainerID: trainerID,
		StartedAt: start,
		EndedAt:   end,
	}
}

func (a Appointment) TimeOverlapsWith(other Appointment) bool {
	return a.TimeRange().Contains(other.StartedAt, timerange.RangeSpec{IncludeStart: true}) ||
		a.TimeRange().Contains(other.EndedAt, timerange.RangeSpec{IncludeEnd: true})
}

func (a Appointment) TimeRange() timerange.TimeRange {
	return timerange.TimeRange{
		Start: a.StartedAt,
		End:   a.EndedAt,
	}
}

func (a Appointment) Duration() time.Duration {
	return a.EndedAt.Sub(a.StartedAt)
}
