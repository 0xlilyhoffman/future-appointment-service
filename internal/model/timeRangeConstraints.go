package model

import (
	"errors"
	"future-appointment-service/pkg/timerange"
	"time"

	"github.com/hashicorp/go-multierror"
)

type TimeRangeConstraints struct {
	ValidDailyTimes   DailyTimeRanges
	ValidDuration     time.Duration
	ValidStartMinutes []int // TODO: []time.Minute?
}

func NewTimeRangeConstraints(dailyTimes DailyTimeRanges, duration time.Duration, startMinutes []int) TimeRangeConstraints {
	return TimeRangeConstraints{
		ValidDailyTimes:   dailyTimes,
		ValidDuration:     duration,
		ValidStartMinutes: startMinutes,
	}
}

func (c TimeRangeConstraints) Validate(timeRange timerange.TimeRange) error {
	var (
		errInvalidDuration     = errors.New("invalid duration")
		errInvalidStartMinutes = errors.New("invalid start time minutes")
		errInvalidStartTime    = errors.New("invalid start time")
		errInvalidEndTime      = errors.New("invalid end time")
		result                 error
	)

	if !c.ValidDailyTimes.Contains(timeRange.Start) {
		result = multierror.Append(result, errInvalidStartTime)
	}

	if !c.ValidDailyTimes.Contains(timeRange.End) {
		result = multierror.Append(result, errInvalidEndTime)
	}

	if timeRange.Duration() != c.ValidDuration {
		result = multierror.Append(result, errInvalidDuration)
	}

	validStartMinutes := false
	for _, m := range c.ValidStartMinutes {
		if timeRange.Start.Minute() == m {
			validStartMinutes = true
			break
		}
	}
	if !validStartMinutes {
		result = multierror.Append(result, errInvalidStartMinutes)
	}

	return result
}
