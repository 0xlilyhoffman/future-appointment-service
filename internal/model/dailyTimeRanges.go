package model

import (
	"future-appointment-service/pkg/clocktime"
	"future-appointment-service/pkg/timerange"
	"time"
)

type DailyTimeRanges map[time.Weekday]clocktime.ClockTimeRange

func (b DailyTimeRanges) Contains(t time.Time) bool {
	hoursForDay, ok := b[t.Weekday()]
	if !ok {
		return false
	}

	dayStartTime := hoursForDay.Start.TimeRelativeTo(t)
	dayEndTime := hoursForDay.End.TimeRelativeTo(t)
	dayTimeRange := timerange.NewTimeRange(dayStartTime, dayEndTime)

	return dayTimeRange.Contains(t, timerange.RangeSpec{IncludeStart: true, IncludeEnd: true})
}
