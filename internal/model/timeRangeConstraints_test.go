package model

import (
	"future-appointment-service/pkg/clocktime"
	"future-appointment-service/pkg/timerange"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestTimeRangeConstraints(t *testing.T) {
	var (
		dayStart, _   = clocktime.New(8, 0)  // ignoring error for hard coded valid time
		dayEnd, _     = clocktime.New(17, 0) // ignoring error for hard coded valid time
		businessHours = DailyTimeRanges{
			time.Monday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Tuesday:   clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Wednesday: clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Thursday:  clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Friday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Saturday:  clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Sunday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
		}
		appointmentDuration  = time.Minute * 30
		validStartMinutes    = []int{0, 30}
		timeRangeConstraints = NewTimeRangeConstraints(businessHours, appointmentDuration, validStartMinutes)
		err                  error
	)

	now := time.Now()
	referenceTime := time.Date(now.Year(), now.Month(), now.Day(), dayStart.TimeRelativeTo(now).Hour(), 0, 0, 0, now.Location())
	validTimeRange := timerange.NewTimeRange(referenceTime, referenceTime.Add(appointmentDuration))

	t.Run("Validate", func(t *testing.T) {
		// Valid
		timeRange := validTimeRange
		err = timeRangeConstraints.Validate(timeRange)
		require.Nil(t, err)

		// Invalid Duration
		timeRange = validTimeRange
		timeRange.End = timeRange.End.Add(time.Minute)
		err = timeRangeConstraints.Validate(timeRange)
		require.NotNil(t, err)

		// Invalid Minutes
		timeRange = validTimeRange
		timeRange.Start = timeRange.Start.Add(time.Minute)
		err = timeRangeConstraints.Validate(timeRange)
		require.NotNil(t, err)

		// Invalid Start Time
		timeRange = validTimeRange
		timeRange.Start = dayStart.TimeRelativeTo(now).Add(-time.Hour)
		err = timeRangeConstraints.Validate(timeRange)
		require.NotNil(t, err)

		// Invalid End Time
		timeRange = validTimeRange
		timeRange.End = dayEnd.TimeRelativeTo(now).Add(time.Hour)
		err = timeRangeConstraints.Validate(timeRange)
		require.NotNil(t, err)
	})
}
