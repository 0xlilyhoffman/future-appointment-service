package model

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestAppointment(t *testing.T) {
	now := time.Now()
	referenceTime := time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 0, 0, 0, now.Location())
	appointmentDuration := time.Minute * 30
	referenceAppointment := NewAppointment(0, 0, referenceTime, referenceTime.Add(appointmentDuration))

	t.Run("OverlapsWith", func(t *testing.T) {
		testAppointment := referenceAppointment

		// Case: Ends before start of reference appointment - No overlap
		// Reference:               |--------|
		// Test:      |--------|
		testAppointment.EndedAt = referenceAppointment.StartedAt.Add(-appointmentDuration * 3)
		testAppointment.StartedAt = testAppointment.EndedAt.Add(-appointmentDuration)
		overlaps := testAppointment.TimeOverlapsWith(referenceAppointment)
		require.False(t, overlaps)

		// Case: Ends at the start of reference appointment - No overlap
		// Reference:          |--------|
		// Test:      |--------|
		testAppointment.EndedAt = referenceAppointment.StartedAt
		testAppointment.StartedAt = testAppointment.EndedAt.Add(-appointmentDuration)
		overlaps = testAppointment.TimeOverlapsWith(referenceAppointment)
		require.False(t, overlaps)

		// Case: Ends in the middle of rerence appointmnet - Overlap
		// Reference:       |--------|
		// Test:      |--------|
		testAppointment.EndedAt = referenceAppointment.StartedAt.Add(time.Minute)
		testAppointment.StartedAt = testAppointment.EndedAt.Add(-appointmentDuration)
		overlaps = testAppointment.TimeOverlapsWith(referenceAppointment)
		require.True(t, overlaps)

		// Case: Exact times - overlap - Overlap
		// Reference: |--------|
		// Test:      |--------|
		testAppointment.StartedAt = referenceAppointment.StartedAt
		testAppointment.EndedAt = referenceAppointment.EndedAt
		overlaps = testAppointment.TimeOverlapsWith(referenceAppointment)
		require.True(t, overlaps)

		// Case: Starts in the middle of reference appointmnet - Overlap
		// Reference: |--------|
		// Test:         |--------|
		testAppointment.StartedAt = referenceAppointment.StartedAt.Add(time.Minute)
		testAppointment.EndedAt = testAppointment.StartedAt.Add(appointmentDuration)
		overlaps = testAppointment.TimeOverlapsWith(referenceAppointment)
		require.True(t, overlaps)

		// Case: Starts at reference end - No overlap
		// Reference: |--------|
		// Test:               |--------|
		testAppointment.StartedAt = referenceAppointment.EndedAt
		testAppointment.EndedAt = testAppointment.StartedAt.Add(appointmentDuration)
		overlaps = testAppointment.TimeOverlapsWith(referenceAppointment)
		require.False(t, overlaps)

		// Case: Starts after end of reference appointment - No overlap
		// Reference: |--------|
		// Test:                    |--------|
		testAppointment.StartedAt = referenceAppointment.EndedAt.Add(time.Hour)
		testAppointment.EndedAt = testAppointment.StartedAt.Add(appointmentDuration)
		overlaps = testAppointment.TimeOverlapsWith(referenceAppointment)
		require.False(t, overlaps)

	})
}
