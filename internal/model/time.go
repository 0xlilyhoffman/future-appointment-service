package model

import (
	"errors"
	"sort"
	"time"
)

// Extension on time.Time

type Time time.Time

func (ext Time) NextTimeWithMinutesIn(minutes []int) (time.Time, error) {
	if len(minutes) == 0 {
		return time.Time{}, errors.New("empty minutes")
	}

	t := time.Time(ext)
	sort.Ints(minutes)

	for _, validStartMinute := range minutes {
		if validStartMinute == t.Minute() {
			return t, nil
		}
		if validStartMinute > t.Minute() {
			offset := validStartMinute - t.Minute()
			nextValidStartTime := t.Add(time.Minute * time.Duration(offset))
			return nextValidStartTime, nil
		}
	}

	offset := int(time.Hour.Minutes()) - t.Minute() + minutes[0]
	nextValidStartTime := t.Add(time.Minute * time.Duration(offset))
	return nextValidStartTime, nil
}
