package model

import (
	"future-appointment-service/pkg/clocktime"
	"time"
)

// Default Future Time Constraints define rules for valid times to schedule appointments
// See: DefaultFutureBusinessHours, DefaultFutureAppointmentDuration, and DefaultFutureAppointmentStartMinutes for details
func DefaultFutureTimeConstraints() (TimeRangeConstraints, error) {
	defaultFutureBusinessHours, err := DefaultFutureBusinessHours()
	if err != nil {
		return TimeRangeConstraints{}, nil
	}

	defaultTimeConstraints := NewTimeRangeConstraints(
		defaultFutureBusinessHours,
		DefaultFutureAppointmentDuration(),
		DefaultFutureAppointmentStartMinutes())
	return defaultTimeConstraints, nil
}

// Default Future Business Hours are M-F 8-5
func DefaultFutureBusinessHours() (DailyTimeRanges, error) {
	start, err := clocktime.New(8, 0)
	if err != nil {
		return DailyTimeRanges{}, err
	}
	end, err := clocktime.New(17, 0)
	if err != nil {
		return DailyTimeRanges{}, err
	}

	businessHours := clocktime.ClockTimeRange{
		Start: start,
		End:   end,
	}

	weekdayBusinessHours := DailyTimeRanges{
		time.Monday:    businessHours,
		time.Tuesday:   businessHours,
		time.Wednesday: businessHours,
		time.Thursday:  businessHours,
		time.Friday:    businessHours,
	}

	return weekdayBusinessHours, nil
}

// Default Future Appointments are 30 minutes long
func DefaultFutureAppointmentDuration() time.Duration {
	return time.Minute * 30
}

// Default Future Appointments start :00 and :30 minutes after the hour
func DefaultFutureAppointmentStartMinutes() []int {
	return []int{0, 30}
}
