package model

import (
	"future-appointment-service/pkg/clocktime"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestDailyTimeRanges(t *testing.T) {
	var (
		dayStart, _     = clocktime.New(8, 0)  // ignoring error for hard coded valid time
		dayEnd, _       = clocktime.New(17, 0) // ignoring error for hard coded valid time
		dailyTimeRanges = DailyTimeRanges{
			time.Monday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Tuesday:   clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Wednesday: clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Thursday:  clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Friday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Saturday:  clocktime.NewClockTimeRange(dayStart, dayEnd),
			// No Sunday
		}
	)

	t.Run("Contains", func(t *testing.T) {
		var (
			validateContains = func(t *testing.T, time time.Time) {
				contains := dailyTimeRanges.Contains(time)
				require.True(t, contains)
			}
			validateDoesNotContain = func(t *testing.T, time time.Time) {
				contains := dailyTimeRanges.Contains(time)
				require.False(t, contains)
			}
		)
		// Start with valid date (not sunday), validate time
		referenceTime := time.Now()
		if referenceTime.Weekday() == time.Sunday {
			referenceTime = referenceTime.Add(time.Hour * 24)
		}

		dayStartTime := dayStart.TimeRelativeTo(referenceTime)
		dayEndTime := dayEnd.TimeRelativeTo(referenceTime)

		validateDoesNotContain(t, dayStartTime.Add(-time.Hour))
		validateContains(t, dayStartTime)
		validateContains(t, dayStartTime.Add(time.Hour))
		validateContains(t, dayEndTime)
		validateDoesNotContain(t, dayEndTime.Add(time.Hour))

		// Check invalid date
		for referenceTime.Weekday() != time.Sunday {
			referenceTime = referenceTime.Add(time.Hour * 24)
		}
		dayStartTime = dayStart.TimeRelativeTo(referenceTime)
		dayEndTime = dayEnd.TimeRelativeTo(referenceTime)
		validateDoesNotContain(t, dayStartTime.Add(-time.Hour))
		validateDoesNotContain(t, dayStartTime)
		validateDoesNotContain(t, dayStartTime.Add(time.Hour))
		validateDoesNotContain(t, dayEndTime)
		validateDoesNotContain(t, dayEndTime.Add(time.Hour))
	})

}
