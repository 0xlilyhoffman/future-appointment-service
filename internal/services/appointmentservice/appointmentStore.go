package appointmentservice

import (
	"future-appointment-service/internal/model"
)

type AppointmentStore interface {
	AddAppointment(a model.Appointment) (int, error)
	GetAppointment(id int) (model.Appointment, error)
	GetAppointments(filter model.AppointmentFilter) ([]model.Appointment, error)
}
