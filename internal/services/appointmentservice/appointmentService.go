package appointmentservice

import (
	"future-appointment-service/internal/model"
	"future-appointment-service/pkg/timerange"
	"time"
)

type AppointmentService interface {
	ScheduleAppointment(a model.Appointment) (model.Appointment, error)
	GetScheduledAppointmentsForTrainer(trainerID int) ([]model.Appointment, error)
	GetAvailableAppointmentsTimesForTrainerInTimeRange(trainerID int, startTime time.Time, endTime time.Time) ([]timerange.TimeRange, error)
}
