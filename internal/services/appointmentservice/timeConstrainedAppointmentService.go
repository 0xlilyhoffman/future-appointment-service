package appointmentservice

import (
	"errors"
	"future-appointment-service/internal/model"
	"future-appointment-service/pkg/timerange"
	"time"
)

var _ AppointmentService = (TimeConstrainedAppointmentService)(TimeConstrainedAppointmentService{})

// TimeConstrainedAppointmentService is an AppointmentService that
// handles appointment scheduling, respecting the provided TimeRangeConstraints
type TimeConstrainedAppointmentService struct {
	timeConstraints model.TimeRangeConstraints
	datastore       AppointmentStore
}

// NewTimeConstrainedAppointmentService returns a new TimeConstrainedAppointmentService
func NewTimeConstrainedAppointmentService(timeConstraints model.TimeRangeConstraints, datastore AppointmentStore) TimeConstrainedAppointmentService {
	return TimeConstrainedAppointmentService{
		timeConstraints: timeConstraints,
		datastore:       datastore,
	}
}

// ScheduleAppointment schedules the provided appointment, if possible
//
// An error is returned and the appointment is not scheduled if
// (1) the appointment time is invalid, or
// (2) the appointment overlaps with scheduled appointments
func (s TimeConstrainedAppointmentService) ScheduleAppointment(a model.Appointment) (model.Appointment, error) {
	// Validate that the appointment matches the time constraints provided
	err := s.timeConstraints.Validate(a.TimeRange())
	if err != nil {
		return model.Appointment{}, err
	}

	// Validate that the appointment fits in the trainer schedule
	filter := model.AppointmentFilter{
		TrainerID: a.TrainerID,
	}

	scheduledAppointments, err := s.datastore.GetAppointments(filter)
	if err != nil {
		return model.Appointment{}, err
	}

	for _, scheduledAppointment := range scheduledAppointments {
		if a.TimeOverlapsWith(scheduledAppointment) {
			return model.Appointment{}, errors.New("appointment overlaps with existing appointment")
		}
	}

	// Schedule appointment!
	id, err := s.datastore.AddAppointment(a)
	if err != nil {
		return model.Appointment{}, err
	}

	appointment, err := s.datastore.GetAppointment(id)
	if err != nil {
		return model.Appointment{}, err
	}

	return appointment, nil
}

// GetScheduledAppointmentsForTrainer returns a list of appointments scheduled for the requested trainer
func (s TimeConstrainedAppointmentService) GetScheduledAppointmentsForTrainer(trainerID int) ([]model.Appointment, error) {
	filter := model.AppointmentFilter{
		TrainerID: trainerID,
	}

	appointments, err := s.datastore.GetAppointments(filter)
	if err != nil {
		return nil, err
	}

	return appointments, nil
}

// GetAvailableAppointmentsTimesForTrainerInTimeRange returns a list of times within the provided range
// that the requested trainer has available openings for
func (s TimeConstrainedAppointmentService) GetAvailableAppointmentsTimesForTrainerInTimeRange(trainerID int, startTime time.Time, endTime time.Time) ([]timerange.TimeRange, error) {
	filter := model.AppointmentFilter{
		TrainerID:        trainerID,
		StartedAtOrAfter: startTime,
		EndedAtOrBefore:  endTime,
	}

	scheduledAppointments, err := s.datastore.GetAppointments(filter)
	if err != nil {
		return nil, err
	}

	availableAppointmentTimes := s.findAvailableAppointmentTimesInRange(timerange.NewTimeRange(startTime, endTime), scheduledAppointments)
	return availableAppointmentTimes, nil
}

func (s TimeConstrainedAppointmentService) findAvailableAppointmentTimesInRange(timeRange timerange.TimeRange, scheduledAppointments []model.Appointment) []timerange.TimeRange {
	appointmentTimes := []timerange.TimeRange{}

	start, err := model.Time(timeRange.Start).NextTimeWithMinutesIn(s.timeConstraints.ValidStartMinutes)
	if err != nil {
		return appointmentTimes
	}

	for start.Before(timeRange.End) {
		appointmentTime := timerange.TimeRange{
			Start: start,
			End:   start.Add(s.timeConstraints.ValidDuration),
		}

		isInTimeRange := timeRange.Contains(appointmentTime.Start, timerange.RangeSpec{IncludeStart: true, IncludeEnd: true}) &&
			timeRange.Contains(appointmentTime.End, timerange.RangeSpec{IncludeStart: true, IncludeEnd: true})

		isInBusinessHours := s.timeConstraints.ValidDailyTimes.Contains(appointmentTime.Start) &&
			s.timeConstraints.ValidDailyTimes.Contains(appointmentTime.End)

		overlapsScheduledAppointment := false
		potentialAppointment := model.NewAppointment(0, 0, appointmentTime.Start, appointmentTime.End)
		for _, scheduledAppointment := range scheduledAppointments {
			if scheduledAppointment.TimeOverlapsWith(potentialAppointment) {
				overlapsScheduledAppointment = true
				break
			}
		}

		if isInTimeRange && isInBusinessHours && !overlapsScheduledAppointment {
			appointmentTimes = append(appointmentTimes, appointmentTime)
		}

		start, err = model.Time(start.Add(time.Minute)).NextTimeWithMinutesIn(s.timeConstraints.ValidStartMinutes)
		if err != nil {
			return appointmentTimes
		}
	}

	return appointmentTimes
}
