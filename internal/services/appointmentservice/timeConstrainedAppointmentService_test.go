package appointmentservice

import (
	"future-appointment-service/internal/model"
	"future-appointment-service/pkg/clocktime"
	"future-appointment-service/pkg/timerange"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

var _ AppointmentStore = (MockAppointmentStore)(MockAppointmentStore{})

type MockAppointmentStore struct{}

func (m MockAppointmentStore) AddAppointment(a model.Appointment) (int, error) {
	return 0, nil
}

func (m MockAppointmentStore) GetAppointments(filter model.AppointmentFilter) ([]model.Appointment, error) {
	return nil, nil
}

func (m MockAppointmentStore) GetAppointment(id int) (model.Appointment, error) {
	return model.Appointment{}, nil
}

func TestFindAvailableAppointmentTimesInRange(t *testing.T) {
	var (
		// Business Hours (short for testing): 8am-10am
		dayStart, _   = clocktime.New(8, 0)  // ignoring error for hard coded valid time
		dayEnd, _     = clocktime.New(10, 0) // ignoring error for hard coded valid time
		businessHours = model.DailyTimeRanges{
			time.Monday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Tuesday:   clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Wednesday: clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Thursday:  clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Friday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Saturday:  clocktime.NewClockTimeRange(dayStart, dayEnd),
			time.Sunday:    clocktime.NewClockTimeRange(dayStart, dayEnd),
		}
		appointmentDuration = time.Minute * 30
		validStartMinutes   = []int{0, 30}
		timeConstraints     = model.NewTimeRangeConstraints(businessHours, appointmentDuration, validStartMinutes)
		now                 = time.Now()

		// Unit tests do not use database, so ok to use MockAppointmentStore here
		timeConstrainedAppointmentService = NewTimeConstrainedAppointmentService(timeConstraints, MockAppointmentStore{})
	)

	// Create a time range that covers one full day of business hours, with part of the range OUTSIDE business hours
	timeRangeStart := dayStart.TimeRelativeTo(now).Add(-time.Hour)
	timeRangeEnd := dayEnd.TimeRelativeTo(now).Add(time.Hour)
	timeRange := timerange.NewTimeRange(timeRangeStart, timeRangeEnd)

	// Exclude "scheduled appointments": 8:30-9:00, 9:00-9:30
	scheduledAppointments := []model.Appointment{
		model.NewAppointment(0, 0, clocktime.ClockTime{Hours: 8, Minutes: 30}.TimeRelativeTo(now), clocktime.ClockTime{Hours: 9, Minutes: 0}.TimeRelativeTo(now)),
		model.NewAppointment(0, 0, clocktime.ClockTime{Hours: 9, Minutes: 0}.TimeRelativeTo(now), clocktime.ClockTime{Hours: 9, Minutes: 30}.TimeRelativeTo(now)),
	}

	// Validate expected appointment times
	expectedAppointmentTimeRanges := []timerange.TimeRange{
		// 8:00-8:30, 9:30-10:00
		timerange.NewTimeRange(clocktime.ClockTime{Hours: 8, Minutes: 0}.TimeRelativeTo(now), clocktime.ClockTime{Hours: 8, Minutes: 30}.TimeRelativeTo(now)),
		timerange.NewTimeRange(clocktime.ClockTime{Hours: 9, Minutes: 30}.TimeRelativeTo(now), clocktime.ClockTime{Hours: 10, Minutes: 0}.TimeRelativeTo(now)),
	}

	appointmentTimeRanges := timeConstrainedAppointmentService.findAvailableAppointmentTimesInRange(timeRange, scheduledAppointments)
	require.Equal(t, expectedAppointmentTimeRanges, appointmentTimeRanges)
}
