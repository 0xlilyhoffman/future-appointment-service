### Future Appointment Scheduling

##### Hello
Hello Future Reviewer,

This is the Appointment Scheduling service for the backend takehome.

I've implemented the backend REST api and a CLI for manually testing it. Instructions on how to build/use/test are found below. 

*NOTE: It seems to work as desired, but as of now I still have a few more tests I'd like to write.* 

##### Overview
`appointment-service` provides a REST api with the following endpoints
* `GET /appointmnets/{trainer_id}/scheduled`
    * Returns a list of scheduled appointments for the trainer
* `GET /appointments/{trainer_id}/available/{start_at}/{end_at}/`
    * Returns a list of available appointment times for a trainer between two dates
* `POST /appointments`
    * Schedules a new appointment

##### Usage
* Building
    * `make server` to build REST API backend `future-scheduler-server`
    * `make client` to build cli frontend `future-scheduler-client`
* Running
    * `future-scheduler-server server-config.yaml`
    * `future-scheduler-client -h` (uses config: `client-config.yaml`)
* CLI Sample Commands
    * `scheduled-appointments --trainer {n}`
    * `available-times --trainer {n} --start {mm/dd/yyyy-hh:mm-LOC} --end {mm/dd/yyyy-hh:mm-LOC}`
    * `schedule-appointment --trainer {n} --user {n} --start {mm/dd/yyyy-hh:mm-LOC} --end {mm/dd/yyyy-hh:mm-LOC}`
* Testing
    * `make test` runs all tests
    * `make test-coverage` to view test coverage in web browser

###### Sample commmands / outputs
* `future-scheduler-client scheduled-appointments --trainer 1`
* `future-scheduler-client available-times --trainer 1 --start 01/24/2019-06:00-PST --end 01/25/2019-19:00-PST`
* `future-scheduler-client schedule-appointment --trainer 42 --user 99 --start 02/02/2022-14:00-PST --end 02/02/2022-14:30-PST`
    * `future-scheduler-client scheduled-appointments --trainer 42`

![](docs/images/scheduled-appointments.png)
![](docs/images/available-times.png)
![](docs/images/create.png)
![](docs/images/scheduled-appointments-42.png)

##### Details
We define interfaces `AppointmentService` and `AppointmentStore` to express the required functionality and provide a flexible design.
In main, concrete instantiations of each of these are created to build the application.

###### Service 
```
type AppointmentService interface {
	ScheduleAppointment(a Appointment) error
	GetScheduledAppointmentsForTrainer(trainerID int) ([]Appointment, error)
	GetAvailableAppointmentsTimesForTrainerInTimeRange(trainerID int, startTime time.Time, endTime time.Time) ([]TimeRange, error)
}
```
- Implemented by service layer `TimeConstrainedAppointmentService`
- Used by api layer `AppointmentService`

###### Datastorage
```
type AppointmentStore interface {
	AddAppointment(a Appointment) error
	GetAppointments(filter AppointmentFilter) ([]Appointment, error)
}
```
- Implemented by `FileSystemAppointmentStore`
- Used by service layer `AppointmentService`

##### Project Structure
```
Directory Structure Outline

 ┣ 📂cmd                        
 ┃ ┣ 📂client               # Appointment scheduling CLI 
 ┃ ┃ ┗ 📜main.go
 ┃ ┗ 📂server
 ┃ ┃ ┗ 📜main.go            # Appointment scheduling backend server
 ┣ 📂internal               # Application specific logic
 ┃ ┣ 📂infra                # Web / Storage
 ┃ ┃ ┣ 📂input
 ┃ ┃ ┃ ┗ 📂restapi
 ┃ ┃ ┗ 📂output
 ┃ ┃ ┃ ┗ 📂storage
 ┃ ┃ ┃ ┃ ┗ 📂fsstorage
 ┃ ┣ 📂model                # Domain model entities
 ┃ ┗ 📂services             # Business logic use cases
 ┃ ┃ ┗ 📂appointmentservice
 ┣ 📂pkg                    # Utility
 ┃ ┣ 📂clocktime
 ┃ ┗ 📂timerange
 ┣ 📜appointments.json
 ```

 ##### TODO
 * Finish writing tests
 * Improve error messages