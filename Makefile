server:
	go build -o future-scheduler-server ./cmd/server

client:
	go build -o future-scheduler-client ./cmd/client

test:
	MallocNanoZone=0 go test -race -covermode=atomic -coverprofile=testcoverage.out -p 1 ./...

test-coverage:
	go tool cover -html=testcoverage.out
