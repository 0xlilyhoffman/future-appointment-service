package cli

import (
	"errors"
	"fmt"
	"future-appointment-service/cmd/client/appointmentclient"
	"future-appointment-service/internal/model"
	"future-appointment-service/pkg/timerange"
	"os"
	"time"

	"github.com/spf13/cobra"
)

var (
	layout  = "01/02/2006-15:04-MST"
	rootCmd = &cobra.Command{
		Use:  "appointments",
		Long: ``,
		Run: func(cmd *cobra.Command, args []string) {
		},
	}
)

func Execute(appointmentClient appointmentclient.AppointmentClient) {
	rootCmd.AddCommand(ListAvailableTimesCommand(appointmentClient))
	rootCmd.AddCommand(ListScheduledAppointmentsCommand(appointmentClient))
	rootCmd.AddCommand(ScheduleAppointmentCommand(appointmentClient))

	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func ListAvailableTimesCommand(appointmentClient appointmentclient.AppointmentClient) *cobra.Command {
	var (
		trainerID int
		start     string
		end       string
	)
	cmd := cobra.Command{
		Use:   "available-times",
		Short: "available-times --trainer n --start mm/dd/yyyy-hh:mm-LOC --end mm/dd/yyyy-hh:mm-LOC",
		RunE: func(cmd *cobra.Command, args []string) error {
			if trainerID == 0 {
				return errors.New("missing required flag --trainer")
			}
			startAt, err := time.Parse(layout, start)
			if err != nil {
				return err
			}
			endAt, err := time.Parse(layout, end)
			if err != nil {
				return err
			}

			appointmentTimes, err := appointmentClient.GetAvailableAppointmentsTimesForTrainerInTimeRange(trainerID, startAt, endAt)
			if err != nil {
				return err
			}

			PrettyPrintTimeRanges(appointmentTimes)

			return nil
		},
	}
	cmd.Flags().IntVar(&trainerID, "trainer", 0, "trainer id")
	cmd.Flags().StringVar(&start, "start", "", fmt.Sprintf("start date of the form %s", layout))
	cmd.Flags().StringVar(&end, "end", "", fmt.Sprintf("end date of the form %s", layout))

	return &cmd
}

func ListScheduledAppointmentsCommand(appointmentClient appointmentclient.AppointmentClient) *cobra.Command {
	var (
		trainerID int
	)
	cmd := cobra.Command{
		Use:   "scheduled-appointments",
		Short: "scheduled-appointments --trainer n",
		RunE: func(cmd *cobra.Command, args []string) error {
			if trainerID == 0 {
				return errors.New("missing required flag --trainer")
			}

			appointments, err := appointmentClient.GetScheduledAppointmentsForTrainer(trainerID)
			if err != nil {
				return err
			}

			PrettyPrintAppointments(appointments)

			return nil
		},
	}
	cmd.Flags().IntVar(&trainerID, "trainer", 0, "trainer id")

	return &cmd
}

func ScheduleAppointmentCommand(appointmentClient appointmentclient.AppointmentClient) *cobra.Command {
	var (
		trainerID int
		userID    int
		start     string
		end       string
	)
	cmd := cobra.Command{
		Use:   "schedule-appointment",
		Short: "schedule-appointment --trainer n --user n --start mm/dd/yyyy-hh:mm-LOC --end mm/dd/yyyy-hh:mm-LOC",
		RunE: func(cmd *cobra.Command, args []string) error {
			if trainerID == 0 {
				return errors.New("missing required flag --trainer")
			}

			if userID == 0 {
				return errors.New("missing required flag --user")
			}

			startAt, err := time.Parse(layout, start)
			if err != nil {
				return err
			}

			endAt, err := time.Parse(layout, end)
			if err != nil {
				return err
			}

			appointment, err := appointmentClient.ScheduleAppointment(model.NewAppointment(userID, trainerID, startAt, endAt))
			if err != nil {
				return err
			}

			fmt.Println("Successfully Scheduled:")
			PrettyPrintAppointment(appointment)
			return nil
		},
	}
	cmd.Flags().IntVar(&trainerID, "trainer", 0, "trainer id")
	cmd.Flags().IntVar(&userID, "user", 0, "user id")
	cmd.Flags().StringVar(&start, "start", "", fmt.Sprintf("start date of the form %s", layout))
	cmd.Flags().StringVar(&end, "end", "", fmt.Sprintf("end date of the form %s", layout))

	return &cmd
}

func PrettyPrintAppointment(a model.Appointment) {
	fmt.Printf("ID:%d | User:%d | Trainer:%d | Start:%02d/%02d/%04d %02d:%02d | End:%02d/%02d/%04d %02d:%02d\n",
		a.ID,
		a.UserID,
		a.TrainerID,
		a.StartedAt.Month(), a.StartedAt.Day(), a.StartedAt.Year(), a.StartedAt.Hour(), a.StartedAt.Minute(),
		a.EndedAt.Month(), a.EndedAt.Day(), a.EndedAt.Year(), a.EndedAt.Hour(), a.EndedAt.Minute())
}

func PrettyPrintAppointments(as []model.Appointment) {
	for _, a := range as {
		PrettyPrintAppointment(a)
	}
}

func PrettyPrintTimeRange(timeRange timerange.TimeRange) {
	start := timeRange.Start
	end := timeRange.End

	fmt.Printf("%02d/%02d/%04d %02d:%02d  -  %02d/%02d/%04d %02d:%02d\n",
		start.Month(), start.Day(), start.Year(), start.Hour(), start.Minute(),
		end.Month(), end.Day(), end.Year(), end.Hour(), end.Minute())
}

func PrettyPrintTimeRanges(timeRanges []timerange.TimeRange) {
	for _, t := range timeRanges {
		PrettyPrintTimeRange(t)
	}
}
