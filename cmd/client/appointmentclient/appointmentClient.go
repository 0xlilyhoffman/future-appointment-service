package appointmentclient

import (
	"fmt"
	"future-appointment-service/cmd/client/httpclient"
	"future-appointment-service/internal/infra/entrypoints/restapi"
	"future-appointment-service/internal/model"
	"future-appointment-service/pkg/timerange"
	"time"
)

type AppointmentClient struct {
	httpclient httpclient.Client
	timeFormat string
	address    string
}

func NewAppointmentClient(httpclient httpclient.Client, timeFormat string, address string) AppointmentClient {
	return AppointmentClient{
		httpclient: httpclient,
		timeFormat: timeFormat,
		address:    address,
	}
}

func (c AppointmentClient) ScheduleAppointment(a model.Appointment) (model.Appointment, error) {
	var (
		url = fmt.Sprintf("%s/appointments", c.address)
		req = restapi.CreateAppointmentRequest{
			TrainerID: a.TrainerID,
			UserID:    a.UserID,
			StartsAt:  a.StartedAt.Format(c.timeFormat),
			EndsAt:    a.EndedAt.Format(c.timeFormat),
		}
		resp = restapi.CreateAppointmentResponse{}
	)

	err := c.httpclient.Post(url, req, &resp)
	if err != nil {
		return model.Appointment{}, err
	}

	return resp.Appointment, nil
}

func (c AppointmentClient) GetScheduledAppointmentsForTrainer(trainerID int) ([]model.Appointment, error) {
	var (
		url  = fmt.Sprintf("%s/appointments/%d/scheduled", c.address, trainerID)
		req  = struct{}{}
		resp = restapi.AppointmentsResponse{}
	)

	err := c.httpclient.Get(url, req, &resp)
	if err != nil {
		return nil, err
	}

	return resp.Appointments, nil
}

func (c AppointmentClient) GetAvailableAppointmentsTimesForTrainerInTimeRange(trainerID int, startTime time.Time, endTime time.Time) ([]timerange.TimeRange, error) {
	var (
		url  = fmt.Sprintf("%s/appointments/%d/available/%s/%s", c.address, trainerID, startTime.Format(c.timeFormat), endTime.Format(c.timeFormat))
		req  = struct{}{}
		resp = restapi.AvailableAppointmentTimesResponse{}
	)
	err := c.httpclient.Get(url, req, &resp)
	if err != nil {
		return nil, err
	}

	return resp.AppointmentTimes, nil
}
