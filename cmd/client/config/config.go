package config

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	ServerAddress string `yaml:"serverAddress"`
}

func LoadConfig(filepath string) (Config, error) {
	config := Config{}

	data, err := os.ReadFile(filepath)
	if err != nil {
		return config, err
	}

	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return config, err
	}

	return config, nil
}
