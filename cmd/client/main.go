package main

import (
	"future-appointment-service/cmd/client/appointmentclient"
	"future-appointment-service/cmd/client/cli"
	"future-appointment-service/cmd/client/config"
	"future-appointment-service/cmd/client/httpclient"
	"log"
)

const (
	timeFormat     = "2006-01-02T15:04:05-07:00"
	configFilepath = "client-config.yaml"
)

func main() {
	config, err := config.LoadConfig(configFilepath)
	if err != nil {
		log.Fatalln(err)
	}

	appointmentClient := appointmentclient.NewAppointmentClient(httpclient.NewClient(), timeFormat, config.ServerAddress)
	cli.Execute(appointmentClient)
}
