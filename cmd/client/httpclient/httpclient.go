package httpclient

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Client struct{}

func NewClient() Client {
	return Client{}
}

func (c Client) Get(url string, req interface{}, resp interface{}) error {
	return c.request(http.MethodGet, url, req, resp)
}

func (c Client) Post(url string, req interface{}, resp interface{}) error {
	return c.request(http.MethodPost, url, req, resp)
}

// request sends the request
func (c Client) request(method string, url string, request interface{}, response interface{}) error {
	if method != http.MethodGet && method != http.MethodPost {
		return fmt.Errorf("invalid method: %s", method)
	}

	data, err := json.Marshal(request)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	if (method == http.MethodGet && resp.StatusCode != http.StatusOK) ||
		(method == http.MethodPost && resp.StatusCode != http.StatusCreated) {
		return errors.New("unexpected status code")
	}

	err = json.Unmarshal(body, &response)
	if err != nil {
		return err
	}

	return nil
}
