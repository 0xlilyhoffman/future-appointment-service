package config

import (
	"errors"
	"fmt"
	"future-appointment-service/internal/model"
	"future-appointment-service/pkg/clocktime"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/hashicorp/go-multierror"
	yaml "gopkg.in/yaml.v2"
)

type Config struct {
	Server             ServerConfig             `yaml:"server"`
	Storage            FSStorageConfig          `yaml:"storage"`
	AppointmentService AppointmentServiceConfig `yaml:"appointmentService"`
}

func (c Config) Validate() error {
	var result error

	err := c.Server.Validate()
	if err != nil {
		result = multierror.Append(result, err)
	}

	err = c.Storage.Validate()
	if err != nil {
		result = multierror.Append(result, err)
	}

	err = c.AppointmentService.Validate()
	if err != nil {
		result = multierror.Append(result, err)
	}

	return result
}

type ServerConfig struct {
	ListenPort int `yaml:"listenPort"`
}

func (c ServerConfig) Validate() error {
	if c.ListenPort == 0 {
		return errors.New("missing requied field: listenPort")
	}
	return nil
}

type FSStorageConfig struct {
	AppointmentsFilepath string `yaml:"appointmentsFilepath"`
}

func (c FSStorageConfig) Validate() error {
	if c.AppointmentsFilepath == "" {
		return errors.New("missing required field: appointmentsFilepath")
	}

	return nil
}

type AppointmentServiceConfig struct {
	BusinessHours              map[string]string `yaml:"businessHours"`
	AppointmentDurationMinutes int               `yaml:"appointmentDurationMinutes"`
	AppointmentStartMinutes    []int             `yaml:"appointmentStartMinutes"`
}

func (c AppointmentServiceConfig) Validate() error {
	var result error

	if c.AppointmentDurationMinutes == 0 {
		result = multierror.Append(result, errors.New("missing value for field: appointmentDurationMinutes"))
	}

	if len(c.AppointmentStartMinutes) == 0 {
		result = multierror.Append(result, errors.New("missing required field: appointmentStartMinutes"))
	}

	if len(c.BusinessHours) == 0 {
		result = multierror.Append(result, errors.New("missing required field: businessHours"))
	}

	return result
}

func (a AppointmentServiceConfig) Constraints() (model.TimeRangeConstraints, error) {
	parseHours := func(hours string) (clocktime.ClockTimeRange, error) {
		regex, err := regexp.Compile(`(\d+):(\d{2})-(\d+):(\d{2})`)
		if err != nil {
			return clocktime.ClockTimeRange{}, err
		}
		matches := regex.FindStringSubmatch(hours)
		if len(matches) != 5 {
			return clocktime.ClockTimeRange{}, fmt.Errorf("invalid hour format %s", hours)
		}

		matches = matches[1:] // only use submatches
		intMatches := []int{}
		for _, m := range matches {
			intMatch, err := strconv.Atoi(m)
			if err != nil {
				return clocktime.ClockTimeRange{}, err
			}

			intMatches = append(intMatches, intMatch)
		}

		rangeStart, err := clocktime.New(intMatches[0], intMatches[1])
		if err != nil {
			return clocktime.ClockTimeRange{}, err
		}

		rangeEnd, err := clocktime.New(intMatches[2], intMatches[3])
		if err != nil {
			return clocktime.ClockTimeRange{}, err
		}

		clockTimeRange := clocktime.NewClockTimeRange(rangeStart, rangeEnd)
		return clockTimeRange, nil
	}

	dailyTimes := model.DailyTimeRanges{}
	emptyTimeRange := clocktime.ClockTimeRange{}
	for day, hours := range a.BusinessHours {
		timeRange, err := parseHours(hours)
		if err != nil {
			return model.TimeRangeConstraints{}, err
		}
		if timeRange == emptyTimeRange {
			return model.TimeRangeConstraints{}, fmt.Errorf("invalid time range for %s", day)
		}

		switch day {
		case time.Monday.String():
			dailyTimes[time.Monday] = timeRange
		case time.Tuesday.String():
			dailyTimes[time.Tuesday] = timeRange
		case time.Wednesday.String():
			dailyTimes[time.Wednesday] = timeRange
		case time.Thursday.String():
			dailyTimes[time.Thursday] = timeRange
		case time.Friday.String():
			dailyTimes[time.Friday] = timeRange
		case time.Saturday.String():
			dailyTimes[time.Saturday] = timeRange
		case time.Sunday.String():
			dailyTimes[time.Sunday] = timeRange
		}
	}

	constraints := model.NewTimeRangeConstraints(
		dailyTimes,
		time.Duration(a.AppointmentDurationMinutes)*time.Minute,
		a.AppointmentStartMinutes)

	return constraints, nil
}

func LoadConfig(filepath string) (Config, error) {
	config := Config{}

	data, err := os.ReadFile(filepath)
	if err != nil {
		return config, err
	}

	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return config, err
	}

	return config, nil
}
