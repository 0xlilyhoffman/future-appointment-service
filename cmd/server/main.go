package main

import (
	"fmt"
	"future-appointment-service/cmd/server/config"
	"future-appointment-service/internal/infra/dataproviders/storage/fsstorage"
	"future-appointment-service/internal/infra/entrypoints/restapi"
	"future-appointment-service/internal/services/appointmentservice"
	"log"
	"os"
	"os/signal"
	"syscall"
)

const (
	timeFormat = "2006-01-02T15:04:05-07:00"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("Usage: %s <config.yaml>", os.Args[0])
	}
	configFilepath := os.Args[1]

	config, err := config.LoadConfig(configFilepath)
	if err != nil {
		log.Fatalln(err)
	}

	// Setup AppointmentStore datastore
	datastore, err := fsstorage.NewFileSystemAppointmentStore(config.Storage.AppointmentsFilepath, timeFormat)
	if err != nil {
		log.Fatalln(err)
	}

	// Setup AppointmentService service
	timeConstraints, err := config.AppointmentService.Constraints()
	if err != nil {
		log.Fatal(err)
	}

	timeConstrainedAppointmentService := appointmentservice.NewTimeConstrainedAppointmentService(timeConstraints, datastore)

	// Setup REST api service
	appointmentService := restapi.NewAppointmnetService(timeConstrainedAppointmentService, timeFormat)

	// Start server
	log.Printf("Listening on %d...\n", config.Server.ListenPort)

	serverError := make(chan error)
	signals := make(chan os.Signal, 10)
	signal.Notify(signals, os.Interrupt, syscall.SIGHUP)

	go func() {
		serverError <- appointmentService.ListenAndServe(fmt.Sprintf(":%d", config.Server.ListenPort))
	}()

	// Wait for server error or request to terminate
	select {
	case signal := <-signals:
		log.Println("server received signal: ", signal)

	case err := <-serverError:
		log.Println("server exited with error: ", err)
	}

	log.Println("Exiting")
}
