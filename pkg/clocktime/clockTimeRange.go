package clocktime

type ClockTimeRange struct {
	Start ClockTime
	End   ClockTime
}

func NewClockTimeRange(start ClockTime, end ClockTime) ClockTimeRange {
	return ClockTimeRange{
		Start: start,
		End:   end,
	}
}
