package clocktime

import (
	"errors"
	"time"
)

const (
	maxHours   = 23
	maxMinutes = 59
)

var ErrInvalidHours = errors.New("invalid hours")
var ErrInvalidMinutes = errors.New("invalid minutes")

type ClockTime struct {
	Hours   int
	Minutes int
}

func New(hours int, minutes int) (ClockTime, error) {
	var (
		clockTime = ClockTime{}
	)

	if hours > maxHours {
		return clockTime, ErrInvalidHours
	}
	if minutes > maxMinutes {
		return clockTime, ErrInvalidMinutes
	}

	clockTime = ClockTime{
		Hours:   hours,
		Minutes: minutes,
	}
	return clockTime, nil
}

func (c ClockTime) TimeRelativeTo(t time.Time) time.Time {
	return time.Date(
		t.Year(), t.Month(), t.Day(),
		c.Hours, c.Minutes,
		t.Second(), t.Nanosecond(), t.Location())
}
