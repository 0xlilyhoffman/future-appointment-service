package timerange

import (
	"time"
)

type TimeRange struct {
	Start time.Time
	End   time.Time
}

func NewTimeRange(start time.Time, end time.Time) TimeRange {
	return TimeRange{
		Start: start,
		End:   end,
	}
}

func (r TimeRange) Duration() time.Duration {
	return r.End.Sub(r.Start)
}

type RangeSpec struct {
	IncludeStart bool
	IncludeEnd   bool
}

func (r TimeRange) Contains(t time.Time, spec RangeSpec) bool {
	contains := r.Start.Before(t) && r.End.After(t)
	if spec.IncludeStart {
		contains = contains || t == r.Start
	}
	if spec.IncludeEnd {
		contains = contains || t == r.End
	}
	return contains
}
